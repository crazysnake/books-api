<?php

use PhpParser\Builder\Namespace_;

namespace App\Traits;

use Illuminate\Http\Response;

trait ApiResponser
{
    /**
     * Return the success response
     * @param string|array $data
     * @param int $code
     * @return \Illuminate\Http\JsonResponse
     */
    public function successResponse ( $data, $code = Response::HTTP_OK) {
        return response()->json(['data' => $data], $code);
    }

    /**
     * Return the error response
     * @param string|array $message
     * @param int $code
     * @return \Illuminate\Http\JsonResponse
     */
    public function errorResponse ( $message, $code) {
        return response()->json(['error' => $message, 'code' => $code], $code );
    }

}


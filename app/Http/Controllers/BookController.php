<?php

namespace App\Http\Controllers;

use App\Models\Book;
use App\Traits\ApiResponser;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class BookController extends Controller
{
    use ApiResponser;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Return the list of Books
     * @return Illuminate/Http/Response
     */
    public function index() {
        $books = Book::all();

        return $this->successResponse($books);
    }

    /**
     * Creates new Book
     * @return Illuminate/Http/Response
     */
    public function store(Request $request) {
        $rules = [
            'title' => 'required|max:255',
            'description' => 'required|max:255',
            'price'=> 'required|min:1',
            'author_id'=> 'required|min:1',
        ];

        $this->validate($request, $rules);

        $book = Book::Create($request->all());

        return $this->successResponse($book, Response::HTTP_CREATED);
    }

    /**
     * Shows Book details
     * @return Illuminate/Http/Response
     */
    public function show($book) {
        $book = Book::findOrFail($book);

        return $this->successResponse($book);
    }

    /**
     * Updates Book details
     * @param \Illuminate\Http\Request $request
     * @param mixed $book
     * @return Illuminate/Http/Response
     */
    public function update(Request $request, $book) {
        $rules = [
            'name' => 'max:255',
            'gender' => 'max:255|in:male,female',
            'country'=> 'max:255',
        ];

        $this->validate($request, $rules);

        $existingBook = Book::findOrFail($book);

        $existingBook->fill($request->all());

        if ($existingBook->isClean()) {
            return $this->errorResponse('At least one value needs to change.', Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        $existingBook->save();

        return $this->successResponse($existingBook);

    }

     /**
     * Removes Book
     * @return Illuminate/Http/Response
     */
    public function destroy($book) {
        $book = Book::findOrFail($book);

        $book->delete();

        $this->successResponse($book);
    }
}
